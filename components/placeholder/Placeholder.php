<?php


namespace yii2portal\menu\components\placeholder;

use Yii;
use yii\base\Component;
use yii\helpers\ArrayHelper;
use yii\widgets\Menu;

class Placeholder extends Component
{

    public function render($key, $options = [])
    {

        $menus = Yii::$app->modules['menu']->getPlaceholder($key);
        $positionsConfig = Yii::$app->modules['menu']->positionsConfig;

        $menu = count($menus) == 1 ? array_shift($menus) : [
            'items' => $menus
        ];

        if(isset($positionsConfig[$key])){

            $options = ArrayHelper::merge($positionsConfig[$key], $options);

        }

        $currentPage = Yii::$app->getModule('structure')->currentPage;
        $pagesTree = $currentPage->parents;
        $pagesTree[] = $currentPage;

        $parentIds = ArrayHelper::getColumn($pagesTree, 'id');


        $activate = function(&$items) use (&$activate, $parentIds){
            foreach($items as $i=>$item){
                if(isset($item['pageId'])){
                    $items[$i]['active'] = in_array($item['pageId'], $parentIds);
                }
                if(!empty($items[$i]['items'])){
                    $activate($items[$i]['items']);
                }
            }
        };
        $activate($menu['items']);
        

        $config = ArrayHelper::merge($menu, $options);

        return Menu::widget($config);
    }




}