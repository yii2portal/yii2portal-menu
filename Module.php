<?php

namespace yii2portal\menu;

use Yii;
use yii\base\BootstrapInterface;
use yii\base\Exception;
use yii\helpers\ArrayHelper;
use yii2portal\menu\models\CoreMenu;
use yii2portal\structure\models\CoreStructure;

class Module extends \yii2portal\core\Module
{

    public $positionsConfig = [];

    private $_menus = null;

    public function getPlaceholder($placeholder)
    {
        if($this->_menus == null){
            $this->getMenu();
        }

        return $this->_menus[$placeholder];
    }


    public function getMenu()
    {
        $menus = CoreMenu::find()
            ->with('items')
            ->all();

        foreach ($menus as $menu) {
            $this->_menus[$menu->place_key][] = $this->_generateMenu($menu);
        }
    }


    private function _generateMenu(CoreMenu $menu)
    {
        $return = [
            'items' => [
            ],
        ];
        $items = $menu->items;
        ArrayHelper::multisort($items, ['ord', 'level'], [SORT_ASC, SORT_ASC]);

        $fstLevel = array_filter($items, function ($item) {
            return $item->level == 0;
        });

        foreach ($fstLevel as $item) {
            $return['items'][] = $this->renderItem($items, $item);
        }

        return $return;
    }


    private function renderItem($items, $item)
    {
        $return = "";

        $structure = Yii::$app->modules['structure'];

        switch ($item->type) {
            case "delim":
                $return = '<li class="divider"></li>';
                break;
            case "link":
                $return = [
                    'label' => $item->title,
                    'url' => $item->url,
                    'items' => []
                ];

                $subItems = array_filter($items, function ($subItem) use ($item) {
                    return $subItem->level == $item->level + 1 && $subItem->pid == $item->id;
                });

                foreach ($subItems as $subItem) {
                    $return['items'][] = $this->renderItem($items, $subItem);
                }

                break;
            default:
                $page = $structure->getPage($item->type);
                if($page) {
                    $return = [
                        'label' => $page->title,
                        'pageId' => $page->id,
                        'url' => $page->urlPath,
                        'items' => []
                    ];

                    $return['url'] = $page->urlPath;


                    $subItems = array_filter($items, function ($subItem) use ($item) {
                        return $subItem->level == $item->level + 1 && $subItem->pid == $item->id;
                    });

                    foreach ($subItems as $subItem) {
                        $return['items'][] = $this->renderItem($items, $subItem);
                    }
                }
                break;
        }


        return $return;
    }
}