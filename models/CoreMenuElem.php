<?php

namespace yii2portal\menu\models;

use Yii;

/**
 * This is the model class for table "core_menu_elem".
 *
 * @property integer $id
 * @property integer $elem_id
 * @property integer $menu_id
 * @property integer $pid
 * @property string $type
 * @property integer $level
 * @property integer $ord
 * @property string $url
 * @property string $title
 * @property string $color
 */
class CoreMenuElem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core_menu_elem';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['elem_id', 'menu_id', 'pid', 'level', 'ord'], 'integer'],
            [['type'], 'string', 'max' => 10],
            [['url', 'title'], 'string', 'max' => 255],
            [['color'], 'string', 'max' => 6],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'elem_id' => 'Elem ID',
            'menu_id' => 'Menu ID',
            'pid' => 'Pid',
            'type' => 'Type',
            'level' => 'Level',
            'ord' => 'Ord',
            'url' => 'Url',
            'title' => 'Title',
            'color' => 'Color',
        ];
    }
}
