<?php

namespace yii2portal\menu\models;

use Yii;

/**
 * This is the model class for table "core_menu".
 *
 * @property integer $id
 * @property string $name
 * @property string $place_key
 * @property string $tpl
 * @property string $struct_sel
 * @property string $olang
 */
class CoreMenu extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core_menu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['struct_sel'], 'string'],
            [['name', 'place_key', 'tpl'], 'string', 'max' => 255],
            [['olang'], 'string', 'max' => 5],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'place_key' => 'Place Key',
            'struct_sel' => 'Struct Sel',
            'olang' => 'Olang',
        ];
    }


    public function getItems(){
        return $this->hasMany(CoreMenuElem::className(), ['menu_id' => 'id']);
    }
}
